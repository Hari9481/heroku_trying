from . import views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url


urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('post/<int:pk>/', views.post_detail, name='post_detail'),
    path('post/post_edit/', views.post_edit, name='post_edit'),
    path('post/new/', views.post_new, name='post_new'),
    path('post/<int:pk>/edit.html', views.post_edit, name='post_edit'),
    #path("register/", views.register, name="register"),
    path("login/", views.login_request, name="login"),
    url(r"^logout/$", views.logout_request, name="logout"),
]